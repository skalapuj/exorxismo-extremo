#ifndef _HABITACION_H_
#define _HABITACION_H_

#include <iostream>
#include <array>
#include <set>
#include <vector>
#include "TiposJuego.h"

using namespace std;


class Habitacion {
public:
    Habitacion(unsigned int tam, set<Pos> ocupadas);

    unsigned int tam() const;

    bool ocupado(Pos) const;

    bool recorrido(Pos) const;

    void marcar(Pos);

    void desmarcar(Pos);

    bool operator==(const Habitacion&) const;

    Pos mover(Pos p, Dir d) const;

private:

    struct casillero {
        casillero(bool o, bool r) : ocupado(o), recorrido(r) {};

        bool ocupado;
        bool recorrido;
    };

    vector<vector<casillero>> _grilla;
    unsigned int _tam;

    void ocupar(Pos p);
};


Habitacion string_to_hab(std::istream& is);
Habitacion string_to_hab(string s);

#endif
