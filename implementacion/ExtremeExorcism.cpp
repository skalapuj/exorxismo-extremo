#include "ExtremeExorcism.h"

ExtremeExorcism::ExtremeExorcism(Habitacion h, set<Jugador> jugadores, PosYDir f_init,
list<Accion> acciones_fantasma, Contexto *ctx) : _pos_fantasma_especial(f_init), _hab(h), _ronda(0), _step(0), _conj_jugadores(jugadores), _ctx(ctx) {
    _fantasma_especial = crearFantasma(f_init, acciones_fantasma);
    list<info_jugador*> l;
    for (int i = 0; i < h.tam(); i++) {
        vector<list<info_jugador*>> v;
        for (int j = 0; j < h.tam(); j++) {
            v.push_back(l);
        }
        jugadoresPorPosicion.push_back(v);
    }
    list<Fantasma> l_f;
    l_f.push_back(_fantasma_especial);
    map<Jugador, PosYDir> loc = ctx->Contexto::localizar_jugadores(jugadores, l_f, h);
    typename set<Jugador>::const_iterator it = jugadores.begin();
    while (it != jugadores.end()) {
        PosYDir pyd = loc[*it];
        info_jugador info = info_jugador(true, pyd.pos, pyd.dir, {Evento(pyd.pos, pyd.dir, false)}, *it);
        _jugadores[*it] = info;
        _info_jugadores_vivos.fast_insert(make_pair(*it, &_jugadores[*it]));
        _pos_jugadores_vivos.fast_insert(make_pair(*it, pyd));
        agregarJugador(&_jugadores[*it], pyd.pos, jugadoresPorPosicion);
        ++it;
    }
}

void ExtremeExorcism::pasar() {
    _step++;
    typename list<PosYDir>::const_iterator it1 = _pos_fantasmas_vivos.begin();
    while(it1 != _pos_fantasmas_vivos.end()) {
        it1 = _pos_fantasmas_vivos.erase(it1);
    }
    typename list<Fantasma*>::const_iterator it2 = _fantasmas_vivos.begin();
    while (it2 != _fantasmas_vivos.end()) {
        int i = _step % (**it2).size();
        typename Fantasma::const_iterator it3 = (**it2).begin();
        for (int j = 0; j < i; j++) {
            ++it3;
        }
        Pos p = (*it3).pos;
        Dir d = (*it3).dir;
        bool b = (*it3).dispara;
        PosYDir pyd = PosYDir(p, d);
        _pos_fantasmas_vivos.push_back(pyd);
        if (b) {
            Pos sig = _hab.mover(p, d);
            while ( sig != p) {
                list<info_jugador*>* l = &listaJug(sig, jugadoresPorPosicion);
                if (!l->empty()) {
                    typename list<info_jugador*>::const_iterator itL = l->begin();
                    while (itL != l->end()) {
                        (**itL).vive = false;
                        itL = l->erase(itL);
                    }
                }
                p = sig;
                sig = _hab.mover(p, d);
            }
        }
        ++it2;
    }
    int i = _step % _fantasma_especial.size();
    typename Fantasma::const_iterator itF = _fantasma_especial.begin();
    for (int j = 0; j < i; j++) {
        ++itF;
    }
    Pos p = (*itF).pos;
    Dir d = (*itF).dir;
    bool b = (*itF).dispara;
    PosYDir pyd = PosYDir(p, d);
    _pos_fantasma_especial = pyd;
    if (b) {
        Pos sig = _hab.mover(p, d);
        while (sig != p) {
            list<info_jugador*> l = listaJug(sig, jugadoresPorPosicion);
            if (!l.empty()) {
                typename list<info_jugador*>::const_iterator itL = l.begin();
                while (itL != l.end()) {
                    (**itL).vive = false;
                    itL = l.erase(itL);
                }
            }
            p = sig;
            sig = _hab.mover(p, d);
        }
    }
    linear_map<Jugador, PosYDir> pos_nuevo;
    linear_map<Jugador, info_jugador*> info_nuevo;
    typename linear_map<Jugador, info_jugador*>::const_iterator it4 = _info_jugadores_vivos.begin();
    while (it4 != _info_jugadores_vivos.end()) {
        if ((*(*it4).second).vive) {
            Evento e = Evento((*(*it4).second).pos, (*(*it4).second).dir, false);
            (*(*it4).second).eventos.push_back(e);
            info_nuevo.fast_insert(*it4);
            PosYDir pydJ = PosYDir((*(*it4).second).pos, (*(*it4).second).dir);
            pos_nuevo.fast_insert(make_pair((*it4).first, pydJ));
        }
        ++it4;
    }
    _info_jugadores_vivos = info_nuevo;
    _pos_jugadores_vivos = pos_nuevo;
}

void ExtremeExorcism::ejecutarAccion(Jugador j, Accion a) {
    bool cambiaRonda = false;
    info_jugador info = _jugadores.at(j);
    if (a == DISPARAR) {
        Pos p = info.pos;
        Dir d = info.dir;
        Pos posF = _pos_fantasma_especial.pos;
        Pos sig = _hab.mover(p, d);
        while (sig != p && !cambiaRonda) {
            cambiaRonda = posF == sig;
            p = sig;
            sig = _hab.mover(p, d);
        }
    }
    if (cambiaRonda) {
        cambiarRonda(j);
    } else {
        _step++;
        if (a != DISPARAR && a != ESPERAR) {
            Pos p = info.pos;
            Dir d = direccion(a);
            eliminarJugador(j, p, jugadoresPorPosicion);
            p = _hab.mover(p, d);
            agregarJugador(&_jugadores[j], p, jugadoresPorPosicion);
            _jugadores[j].pos = p;
            _jugadores[j].dir = d;
        } else if (a == DISPARAR) {
                Pos p = info.pos;
                Dir d = info.dir;
                Pos sig = _hab.mover(p, d);
                while (sig != p) {
                    typename list<Fantasma*>::const_iterator itF = _fantasmas_vivos.begin();
                    while (itF != _fantasmas_vivos.end()) {
                        int i = _step % (**itF).size();
                        typename Fantasma::const_iterator it = (**itF).begin();
                        for (int k = 0; k < i; k++) {
                            ++it;
                        }
                        if ((*it).pos == sig) {
                            itF = _fantasmas_vivos.erase(itF);
                        } else {
                            ++itF;
                        }
                    }
                    p = sig;
                    sig = _hab.mover(p, d);
                }
        }
        _step--;
        pasar();
    }
}

list<pair<Jugador, PosYDir>> ExtremeExorcism::posicionJugadores() const {
    list<pair<Jugador, PosYDir>> res;
    typename linear_map<Jugador, PosYDir>::const_iterator it = _pos_jugadores_vivos.begin();
    while (it != _pos_jugadores_vivos.end()) {
        res.push_back(*it);
        ++it;
    }
    return res;
}

list<PosYDir> ExtremeExorcism::posicionFantasmas() const {
    list<PosYDir> res = _pos_fantasmas_vivos;
    res.push_back(_pos_fantasma_especial);
    return res;
}

PosYDir ExtremeExorcism::posicionEspecial() const {
    return _pos_fantasma_especial;
}

list<PosYDir> ExtremeExorcism::disparosFantasmas() const {
    list<PosYDir> res;
    typename list<Fantasma*>::const_iterator it = _fantasmas_vivos.begin();
    while (it != _fantasmas_vivos.end()) {
        int i = _step % (**it).size();
        typename Fantasma::const_iterator itF = (**it).begin();
        for (int j = 0; j < i; j++) {
            itF++;
        }
        if ((*itF).dispara) {
            PosYDir pyd = PosYDir((*itF).pos, (*itF).dir);
            res.push_back(pyd);
        }
        it++;
    }
    typename Fantasma::const_iterator itFS = _fantasma_especial.begin();
    int i = _step % _fantasma_especial.size();
    for (int j = 0; j < i; j++) {
        itFS++;
    }
    if ((*itFS).dispara) {
        PosYDir pyd = PosYDir((*itFS).pos, (*itFS).dir);
        res.push_back(pyd);
    }
    return res;
}

set<Pos> ExtremeExorcism::posicionesDisparadas() {
    set<Pos> res;
    typename list<Fantasma*>::const_iterator it = _fantasmas_vivos.begin();
    while (it != _fantasmas_vivos.end()) {
        int i = _step % (**it).size();
        typename Fantasma::const_iterator itF = (**it).begin();
        for (int j = 0; j < i; j++) {
            itF++;
        }
        if ((*itF).dispara) {
            Pos p = (*itF).pos;
            Dir d = (*itF).dir;
            Pos sig = _hab.mover(p, d);
            while (sig != p) {
                res.insert(sig);
                p = sig;
                sig = _hab.mover(p, d);
            }
        }
        it++;
    }
    return res;
}

bool ExtremeExorcism::jugadorVivo(Jugador j) const {
    return _jugadores.at(j).vive;
}

const Habitacion& ExtremeExorcism::habitacion() const {
    return _hab;
}

PosYDir ExtremeExorcism::posicionJugador(Jugador j) const {
    Pos p = _jugadores.at(j).pos;
    Dir d = _jugadores.at(j).dir;
    return PosYDir(p, d);
}

const set<Jugador>& ExtremeExorcism::jugadores() const {
    return _conj_jugadores;
}

const list<Fantasma>& ExtremeExorcism::fantasmas() const {
    return _fantasmas;
}

/**
 * AUXILIARES
 */

void ExtremeExorcism::aplicar(Evento &e, Accion a) {
    if (a == DISPARAR) {
        e.dispara = true;
    } else if (a == MIZQUIERDA) {
        if (e.pos.first > 0) {
            e.pos.first--;
        }
        if (_hab.ocupado(e.pos)) {
            e.pos.first++;
        }
        e.dir = IZQUIERDA;
    } else if (a == MDERECHA) {
        e.pos.first++;
        if (e.pos.first >= _hab.tam() || _hab.ocupado(e.pos)) {
            e.pos.first--;
        }
        e.dir = DERECHA;
    } else if (a == MARRIBA) {
        e.pos.second++;
        if (e.pos.second >= _hab.tam() || _hab.ocupado(e.pos)) {
            e.pos.second--;
        }
        e.dir = ARRIBA;
    } else if (a == MABAJO) {
        if (e.pos.second > 0) {
            e.pos.second--;
        }
        if (_hab.ocupado(e.pos)) {
            e.pos.second++;
        }
        e.dir = ABAJO;
    }
}

Fantasma ExtremeExorcism::crearFantasma(PosYDir pyd, list<Accion> l) {
    Fantasma f;
    Evento ev = Evento(pyd.pos, pyd.dir, false);
    f.push_back(ev);
    typename list<Accion>::const_iterator it = l.begin();
    while(it != l.end()) {
        aplicar(ev, *it);
        f.push_back(ev);
        ++it;
    }
    return armarSecuencia(f);
}

list<info_jugador*>& ExtremeExorcism::listaJug(Pos p, Matriz& m) {
    int i = p.first;
    int j = p.second;
    return m[i][j];
}

void ExtremeExorcism::agregarJugador(info_jugador *info, Pos p, Matriz &m) {
    int i = p.first;
    int j = p.second;
    m[i][j].push_back(info);
}

void ExtremeExorcism::eliminarJugador(Jugador jug, Pos p, Matriz &m) {
    int i = p.first;
    int j = p.second;
    typename list<info_jugador*>::const_iterator it = m[i][j].begin();
    while((**it).nombre != jug) {
        ++it;
    }
    m[i][j].erase(it);
}

void ExtremeExorcism::cambiarRonda(Jugador j) {
    info_jugador* info = &_jugadores.at(j);
    Evento e = Evento((*info).pos, (*info).dir, true);
    (*info).eventos.push_back(e);
    _ronda++;
    _step = 0;
    _fantasmas.push_back(_fantasma_especial);
    list<Fantasma*> fant_vivos_nuevo;
    list<PosYDir> pos_fant_nuevo;
    typename list<Fantasma>::iterator it2 = _fantasmas.begin();
    while (it2 != _fantasmas.end()) {
        fant_vivos_nuevo.push_back(&(*it2));
        Pos p = (*(*it2).begin()).pos;
        Dir d = (*(*it2).begin()).dir;
        PosYDir pyd = PosYDir(p, d);
        pos_fant_nuevo.push_back(pyd);
        ++it2;
    }
    _pos_fantasmas_vivos = pos_fant_nuevo;
    _fantasmas_vivos = fant_vivos_nuevo;
    _fantasma_especial = armarSecuencia((*info).eventos);
    Pos pfe = (*_fantasma_especial.begin()).pos;
    Dir dfe = (*_fantasma_especial.begin()).dir;
    _pos_fantasma_especial = PosYDir(pfe, dfe);
    typename linear_map<Jugador, PosYDir>::const_iterator itJ = _pos_jugadores_vivos.begin();
    while(itJ != _pos_jugadores_vivos.end()) {
        eliminarJugador((*itJ).first, (*itJ).second.pos, jugadoresPorPosicion);
        ++itJ;
    }
    _fantasmas.push_back(_fantasma_especial);
    linear_map<Jugador, info_jugador*> info_nuevo;
    linear_map<Jugador, PosYDir> pos_nuevo;
    map<Jugador, PosYDir> loc = _ctx->Contexto::localizar_jugadores(_conj_jugadores, _fantasmas, _hab);
    _fantasmas.pop_back();
    typename set<Jugador>::const_iterator it3 = _conj_jugadores.begin();
    while (it3 != _conj_jugadores.end()) {
        bool b = true;
        Pos p = loc[*it3].pos;
        Dir d = loc[*it3].dir;
        list<Evento> evs;
        Evento ev = Evento(p, d, b);
        evs.push_back(ev);
        _jugadores[*it3] = info_jugador(b, p, d, evs, *it3);
        info_jugador* punt = &_jugadores.at(*it3);
        info_nuevo.fast_insert(make_pair((*it3),punt));
        pos_nuevo.fast_insert(make_pair((*it3), PosYDir(p, d)));
        agregarJugador(punt, p, jugadoresPorPosicion);
        ++it3;
    }
    _info_jugadores_vivos = info_nuevo;
    _pos_jugadores_vivos = pos_nuevo;
}

Fantasma ExtremeExorcism::armarSecuencia(list<Evento> evs) {
    Fantasma f = evs;
    typename list<Evento>::const_iterator it = evs.end();
    it--;
    Evento esp = Evento((*it).pos, (*it).dir, false);
    for (int i = 0; i < 5; i++){
        f.push_back(esp);
    }
    while (it != evs.begin()) {
        Evento e = Evento((*it).pos, opuesto((*it).dir), (*it).dispara);
        f.push_back(e);
        it--;
    }
    Evento u = Evento((*it).pos, opuesto((*it).dir), (*it).dispara);
    for (int i = 0; i < 6; i++) {
        f.push_back(u);
    }
    return f;
}
