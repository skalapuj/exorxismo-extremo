#ifndef EXTREME_EXORCISM_H
#define EXTREME_EXORCISM_H

#include "Contexto.h"
#include "Habitacion.h"
#include "TiposJuego.h"
#include "modulos_basicos/string_map.h"
#include "modulos_basicos/linear_map.h"
#include <list>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

class ExtremeExorcism {
public:
  ExtremeExorcism(Habitacion h, set<Jugador> jugadores, PosYDir f_init,
                  list<Accion> acciones_fantasma, Contexto *ctx);

  void pasar();

  void ejecutarAccion(Jugador j, Accion a);

  list<pair<Jugador, PosYDir>> posicionJugadores() const;

  list<PosYDir> posicionFantasmas() const;

  PosYDir posicionEspecial() const;

  list<PosYDir> disparosFantasmas() const;

  set<Pos> posicionesDisparadas();

  bool jugadorVivo(Jugador j) const;

  const Habitacion &habitacion() const;

  PosYDir posicionJugador(Jugador j) const;

  const set<Jugador> &jugadores() const;

  const list<Fantasma> &fantasmas() const;

private:
  list<Fantasma> _fantasmas;
  list<Fantasma*> _fantasmas_vivos;
  list<PosYDir> _pos_fantasmas_vivos;
  Fantasma _fantasma_especial;
  PosYDir _pos_fantasma_especial;
  string_map<info_jugador> _jugadores;
  algo2::linear_map<Jugador, info_jugador*> _info_jugadores_vivos;
  algo2::linear_map<Jugador, PosYDir> _pos_jugadores_vivos;
  Matriz jugadoresPorPosicion;
  Habitacion _hab;
  int _ronda;
  int _step;

  set<Jugador> _conj_jugadores;
  Contexto* _ctx;

  void aplicar(Evento& e, Accion a);
  Fantasma crearFantasma(PosYDir pyd, list<Accion> l);
  list<info_jugador*>& listaJug(Pos p, Matriz& m);
  void agregarJugador(info_jugador* info, Pos p, Matriz& m);
  void eliminarJugador(Jugador j, Pos p, Matriz& m);
  void cambiarRonda(Jugador j);
  Fantasma armarSecuencia(list<Evento> evs);

};

#endif
