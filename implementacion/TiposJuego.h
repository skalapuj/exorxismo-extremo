#ifndef TIPOS_JUEGO_H
#define TIPOS_JUEGO_H

#include <string>
#include <list>
#include <utility>
#include <iostream>
#include <vector>

using namespace std;

using Jugador = string;
using IDFantasma = unsigned int;
using Pos = pair<unsigned int, unsigned int>;
using Ronda = unsigned int;


struct Evento;

using Fantasma = list<Evento>;

enum Dir {
    ARRIBA = 0, ABAJO, IZQUIERDA, DERECHA
};

enum Accion {
    MARRIBA, MABAJO, MIZQUIERDA, MDERECHA, DISPARAR, ESPERAR
};

Dir direccion(Accion a);
Dir opuesto(Dir d);

struct PosYDir {
    Pos pos;
    Dir dir;

    PosYDir(Pos, Dir);
    PosYDir() : pos(make_pair(0,0)), dir(ARRIBA) {};

    bool operator==(const PosYDir& o) const;
};

struct Evento {
    Pos pos;
    Dir dir;
    bool dispara;

    Evento(Pos, Dir, bool);
    Evento pasar() const;
    PosYDir pos_y_dir() const;

    bool operator==(const Evento&) const;
};

struct info_jugador {
    bool vive;
    Pos pos;
    Dir dir;
    list<Evento> eventos;
    Jugador nombre;

    info_jugador(bool v, Pos p , Dir d, list<Evento> l, Jugador j) : vive(v), pos(p), dir(d), eventos(l), nombre(j) {};
    info_jugador() : vive(true), pos(make_pair(0,0)), dir(ARRIBA), eventos(list<Evento>()), nombre("null") {};
};

using Matriz = vector<vector<list<info_jugador*>>>;

ostream& operator<<(ostream&, const Evento&);
ostream& operator<<(ostream&, const PosYDir&);

#endif
