#include "Habitacion.h"
#include "assert.h"
#include <istream>
#include <sstream>
#include <string>

Habitacion string_to_hab(std::istream& is) {
    int row = 0;
    int col = 0;
    int max_col = 0;
    int width;
    int height;

    set<Pos> occupied;

    char c;
    while (is.get(c)) {
        if (c == '#') {
            occupied.insert(Pos(col, row));
            col++;
        } else if (c == '\n') {
            row++;
            max_col = std::max(col, max_col);
            col = 0;
        } else {
            col++;
        }
    }
    width = max_col;
    height = row;

    assert(height == width);

    return Habitacion(height, occupied);
}

Habitacion string_to_hab(string s) {
    std::istringstream is(s);
    int row = 0;
    int col = 0;
    int max_col = 0;
    int width;
    int height;

    set<Pos> occupied;

    char c;
    while (is.get(c)) {
        if (c == '#') {
            occupied.insert(Pos(col, row));
            col++;
        } else if (c == '\n') {
            row++;
            max_col = std::max(col, max_col);
            col = 0;
        } else {
            col++;
        }
    }
    width = max_col;
    height = row;

    assert(height == width);

    return Habitacion(height, occupied);
}

Habitacion::Habitacion(unsigned int tam, set<Pos> ocupadas) {
    for (int i = 0; i < tam; i++) {
        vector<casillero> v;
        for (int j = 0; j < tam; j++) {
            casillero c = casillero(false, false);
            v.push_back(c);
        }
        _grilla.push_back(v);
    }
    typename set<Pos>::const_iterator it = ocupadas.begin();
    while (it != ocupadas.end()) {
        ocupar((*it));
        ++it;
    }
    _tam = tam;
}

unsigned int Habitacion::tam() const {
    return _tam;
}

bool Habitacion::ocupado(Pos p) const {
    unsigned int i = p.first;
    unsigned int j = p.second;
    return _grilla[i][j].ocupado;
}

bool Habitacion::recorrido(Pos p) const {
    unsigned int i = p.first;
    unsigned int j = p.second;
    return _grilla[i][j].recorrido;
}

void Habitacion::marcar(Pos p) {
    unsigned int i = p.first;
    unsigned int j = p.second;
    _grilla[i][j].recorrido = true;
}

void Habitacion::desmarcar(Pos p) {
    unsigned int i = p.first;
    unsigned int j = p.second;
    _grilla[i][j].recorrido = false;
}

bool Habitacion::operator==(const Habitacion & o) const {
    bool res = true;
    for (int i = 0; i < _tam; i++) {
        if (!res) {
            break;
        }
        for (int j = 0; j < _tam; j++) {
            if(!res) {
                break;
            }
            Pos p = make_pair(i, j);
            res = _grilla[i][j].ocupado == o.ocupado(p);
        }
    }
    return res;
}

Pos Habitacion::mover(Pos p, Dir d) const {
    unsigned int i = p.first;
    unsigned int j = p.second;
    if (d == ARRIBA && j < _tam - 1) {
        j++;
    } else if (d == ABAJO && j > 0) {
        j--;
    } else if (d == IZQUIERDA && i > 0) {
        i--;
    } else if( d == DERECHA && i < _tam - 1) {
        i++;
    }
    Pos n = make_pair(i, j);
    if (ocupado(n)) {
        return p;
    } else {
        return n;
    }
}

void Habitacion::ocupar(Pos p) {
    unsigned int i = p.first;
    unsigned int j = p.second;
    _grilla[i][j].ocupado = true;
}